﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AffinaIsBitch
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        DotMatrix result = new DotMatrix();
        private List<Point> polygonPoints = new List<Point>();

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            Pen pen = new Pen(Color.Black, 2);
            switch (e.Button)
            {
                case MouseButtons.Left:
                    polygonPoints.Add(new Point(e.X, e.Y));
                    if (polygonPoints.Count > 1)
                    {
                        this.DrawLine(pen, polygonPoints[polygonPoints.Count - 2], polygonPoints[polygonPoints.Count - 1]);
                    }
                    break;

                case MouseButtons.Right:
                    if (polygonPoints.Count > 2)
                    {
                        this.DrawLine(pen, polygonPoints[polygonPoints.Count - 1], polygonPoints[0]);
                        dataGridView1.RowCount = polygonPoints.Count;
                        result.dataGridView1.RowCount = polygonPoints.Count;
                        result.dataGridView2.RowCount = polygonPoints.Count;
                        result.dataGridView3.RowCount = polygonPoints.Count;
                        for (int i = 0; i < polygonPoints.Count; i++)
                        {
                            dataGridView1.Rows[i].Cells[0].Value = polygonPoints[i].X;
                            dataGridView1.Rows[i].Cells[1].Value = polygonPoints[i].Y;
                            result.dataGridView1.Rows[i].Cells[0].Value = polygonPoints[i].X;
                            result.dataGridView1.Rows[i].Cells[1].Value = polygonPoints[i].Y;
                            result.dataGridView2.Rows[i].Cells[0].Value = polygonPoints[i].X;
                            result.dataGridView2.Rows[i].Cells[1].Value = polygonPoints[i].Y;
                            result.dataGridView3.Rows[i].Cells[0].Value = polygonPoints[i].X;
                            result.dataGridView3.Rows[i].Cells[1].Value = polygonPoints[i].Y;
                        }
                    }
                    break;
            }
        }

        private void DrawLine(Pen pen, Point p1, Point p2)
        {
            Graphics G = pictureBox1.CreateGraphics();
            G.DrawLine(pen, p1, p2);
        }
        private void button2_Click(object sender, EventArgs e)
        {
            
            Pen pen = new Pen(Color.Red, 2);
            for (int i = 0; i < polygonPoints.Count; i++)
            {
                polygonPoints[i] = (new Point(polygonPoints[i].X + Convert.ToInt32(textBox1.Text),
                polygonPoints[i].Y + Convert.ToInt32(textBox2.Text)));
            }
            RedrawObject(pen);
            result.dataGridView1.ColumnCount += 2;
            dataGridView1.ColumnCount += 2;
            for (int i = 0; i < polygonPoints.Count; i++)
            {
                dataGridView1.Rows[i].Cells[dataGridView1.Columns.Count - 2].Value = polygonPoints[i].X;
                dataGridView1.Rows[i].Cells[dataGridView1.Columns.Count - 1].Value = polygonPoints[i].Y;
                result.dataGridView1.Rows[i].Cells[result.dataGridView1.Columns.Count - 2].Value = polygonPoints[i].X;
                result.dataGridView1.Rows[i].Cells[result.dataGridView1.Columns.Count - 1].Value = polygonPoints[i].Y;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Graphics g = pictureBox1.CreateGraphics();
            if (pictureBox1 != null)
            {
                g.Clear(Color.White);
            }
            polygonPoints.Clear();
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            dataGridView1.Rows.Clear();
            result.dataGridView1.Rows.Clear();
            result.dataGridView2.Rows.Clear();
            result.dataGridView3.Rows.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Pen pen = new Pen(Color.Blue, 2);
            double alpha = Convert.ToDouble(textBox3.Text) * Math.PI / 180;
            for (int i = 0; i < polygonPoints.Count; i++)
            {
                polygonPoints[i] = (new Point((int)(polygonPoints[i].X * Math.Cos(alpha) - polygonPoints[i].Y * Math.Sin(alpha)),
                (int)(polygonPoints[i].X * Math.Sin(alpha) + polygonPoints[i].Y * Math.Cos(alpha))));
            }
            RedrawObject(pen);
            result.dataGridView2.ColumnCount += 2;
            dataGridView1.ColumnCount += 2;
            for (int i = 0; i < polygonPoints.Count; i++)
            {
                dataGridView1.Rows[i].Cells[dataGridView1.Columns.Count - 2].Value = polygonPoints[i].X;
                dataGridView1.Rows[i].Cells[dataGridView1.Columns.Count - 1].Value = polygonPoints[i].Y;
                result.dataGridView2.Rows[i].Cells[result.dataGridView2.Columns.Count - 2].Value = polygonPoints[i].X;
                result.dataGridView2.Rows[i].Cells[result.dataGridView2.Columns.Count - 1].Value = polygonPoints[i].Y;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Pen pen = new Pen(Color.Purple, 2);
            for (int i = 0; i < polygonPoints.Count; i++)
            {
                polygonPoints[i] = (new Point((int)(polygonPoints[i].X * Convert.ToDouble(textBox5.Text)),
                (int)(polygonPoints[i].Y * Convert.ToDouble(textBox4.Text))));
            }
            RedrawObject(pen);
            result.dataGridView3.ColumnCount += 2;
            dataGridView1.ColumnCount += 2;
            for (int i = 0; i < polygonPoints.Count; i++)
            {
                dataGridView1.Rows[i].Cells[dataGridView1.Columns.Count - 2].Value = polygonPoints[i].X;
                dataGridView1.Rows[i].Cells[dataGridView1.Columns.Count - 1].Value = polygonPoints[i].Y;
                result.dataGridView3.Rows[i].Cells[result.dataGridView3.Columns.Count - 2].Value = polygonPoints[i].X;
                result.dataGridView3.Rows[i].Cells[result.dataGridView3.Columns.Count - 1].Value = polygonPoints[i].Y;
            }
        }

        private void RedrawObject(Pen pen)
        {
            for(int i=0; i<polygonPoints.Count-1;i++)
            {
                this.DrawLine(pen, polygonPoints[i + 1], polygonPoints[i]);
                if (i+1 == polygonPoints.Count - 1)
                {
                    this.DrawLine(pen, polygonPoints[polygonPoints.Count - 1], polygonPoints[0]);
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            result.Show();
        }
    }
}